package ru.t1.oskinea.tm.repository;

import ru.t1.oskinea.tm.api.repository.ITaskRepository;
import ru.t1.oskinea.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null) return Collections.emptyList();
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (!task.getProjectId().equals(projectId)) continue;
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

}
