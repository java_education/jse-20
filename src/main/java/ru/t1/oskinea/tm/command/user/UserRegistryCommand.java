package ru.t1.oskinea.tm.command.user;

import ru.t1.oskinea.tm.api.service.IAuthService;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.model.User;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Registry user.";

    private final String NAME = "user-registry";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
        System.out.println("USER SUCCESSFULLY REGISTERED");
        showUser(user);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
