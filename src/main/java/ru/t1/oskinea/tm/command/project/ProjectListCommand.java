package ru.t1.oskinea.tm.command.project;

import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Show project list.";

    private static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.print("ENTER SORT (");
        System.out.print(Arrays.toString(Sort.values()));
        System.out.print("): ");
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
