package ru.t1.oskinea.tm.command.project;

import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Complete project by id.";

    private static final String NAME = "project-complete-by-id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
