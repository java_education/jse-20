package ru.t1.oskinea.tm.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class FormatUtil {

    private static final long KILOBYTE = 1024;

    private static final long MEGABYTE = KILOBYTE * 1024;

    private static final long GIGABYTE = MEGABYTE * 1024;

    private static final long TERABYTE = GIGABYTE * 1024;

    private static final String NAME_BYTES = "B";

    private static final String NAME_KILOBYTE = "KB";

    private static final String NAME_MEGABYTE = "MB";

    private static final String NAME_GIGABYTE = "GB";

    private static final String NAME_TERABYTE = "TB";

    private static final String SEPARATOR = " ";

    private static BigDecimal convertBytes(final long bytes, final long base) {
        return BigDecimal.valueOf(bytes).divide(BigDecimal.valueOf(base)).setScale(2, RoundingMode.HALF_EVEN);
    }

    public static String formatBytes(final long bytes) {
        if (bytes < KILOBYTE) return bytes + SEPARATOR + NAME_BYTES;
        if (bytes < MEGABYTE) return convertBytes(bytes, KILOBYTE) + SEPARATOR + NAME_KILOBYTE;
        if (bytes < GIGABYTE) return convertBytes(bytes, MEGABYTE) + SEPARATOR + NAME_MEGABYTE;
        if (bytes < TERABYTE) return convertBytes(bytes, GIGABYTE) + SEPARATOR + NAME_GIGABYTE;
        return convertBytes(bytes, TERABYTE) + SEPARATOR + NAME_TERABYTE;
    }

}
